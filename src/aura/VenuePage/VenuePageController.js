({
	Search: function(component, event, helper) {
        
        var searchKeyFld = component.find("searchId");
        var srcValue = searchKeyFld.get("v.value");
        if (srcValue == '' || srcValue == null) {
            // display error message if input value is blank or null
            searchKeyFld.set("v.errors", [{
                message: "Enter Search Keyword."
            }]);
        } else {
            searchKeyFld.set("v.errors", null);
            // call helper method
            helper.SearchHelper(component, event);
        }
    },
    
    editRecord : function(component, event, helper) {
        debugger;
           
        var selectedItem = event.currentTarget;
        var inputsel = selectedItem.dataset.record;
       
    var editRecordEvent = $A.get("e.force:editRecord");
    editRecordEvent.setParams({
         "recordId": inputsel
   });
    editRecordEvent.fire();
       event.preventDefault();
},
    
    clickFunction : function(component,event,helper){
        console.log('on click function');
        debugger;
        console.log(event.target.dataset.record);
        debugger;
         var selectedItem = event.currentTarget;
        var recId = event.target.dataset.record;
        var ListOfjunc=component.get("v.junc");
       
        var record=ListOfjunc[recId];
         component.set("v.juncobj",record);
    },
    createRecord : function (component, event, helper) {
    var createRecordEvent = $A.get("e.force:createRecord");
    createRecordEvent.setParams({
        "entityApiName": "Venue__c"
    });
    createRecordEvent.fire();
         event.preventDefault();
},
    submenu:function(component,event){
            debugger;
      component.set("v.flag1",true);
         var selectedItem = event.currentTarget;
       var inputsel = selectedItem.dataset.record;
         var action = component.get("c.getsubvenueList");
        action.setParams({ "accId" :inputsel });
    
      action.setCallback(this, function(response) {
      
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.junc", response.getReturnValue());
             }
          
          debugger;
         
        });
    $A.enqueueAction(action)
      
    },
     Savenext : function (component, event, helper) {
        debugger;
      var action = component.get("c.savevenue");
        var emps=component.get("v.juncobj");
       if (emps == '' || emps == null) {
           var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "Error!",
                           "title": "Error!",
                          "message": "Please select sub venues",
                               });
                     toastEvent.fire();
           event.preventDefault();
      }else{
           
        action.setParams({ "emp" :emps });
       
          action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            var name = response.getReturnValue();
            var state = response.getState();
              if (state === "SUCCESS"){
              var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "Success!",
                           "title": "Success!",
                          "message": "Record saved Successfully",
                               });
                     toastEvent.fire();
                   
            }
            
        });
       // $A.enqueueAction(action);
     component.set('v.defaultTabId',"venuelayid");
      }
    }
   
    
})