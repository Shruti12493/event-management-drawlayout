<aura:application extends="force:slds">
    <aura:attribute name="eventvenueId" type="String"  />
    <aura:attribute name="eventID" type="String"  />
    <c:VenueLayoutMasterComponent eventvenueId="{!v.eventvenueId}" eventID="{!v.eventID}"/>
</aura:application>